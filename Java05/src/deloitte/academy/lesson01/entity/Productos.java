package deloitte.academy.lesson01.entity;

import java.util.ArrayList;
/**
 * This class 
 * @author Emiliano Oliveros
 *
 */
public class Productos {

	public String categoria;
	public String proveedor;
	public double precio;
	public double total;

	public Productos(String categoria, String proveedor, double precio, double total) {
		super();
		this.categoria = categoria;
		this.proveedor = proveedor;
		this.precio = precio;
		this.total = total;
	}

	public Productos() {
		// TODO Auto-generated constructor stub
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public static void agregar(Productos newProduct) {
		Inventario.listProductos.add(newProduct);

	}

	public static ArrayList<Productos> remover(ArrayList<Productos> existentes, Productos inventario) {
		ArrayList<Productos> listaInventario = new ArrayList<Productos>();
		listaInventario = existentes;

		for (Productos elemento : existentes) {
			if (elemento.getCategoria().equals(inventario.getCategoria())) {
				Inventario.listProductos.remove(inventario);
				break;
			}
		}
		return listaInventario;
	}

	public static void actual(ArrayList<Productos> existentes, Productos inventario) {
		int resta;

		for (Productos elemento : existentes) {
			if (elemento.getCategoria().equals(inventario.getCategoria())) {
				resta = (int) (inventario.getTotal() - 1);

				break;
			}
		}
	}

}
